<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Teacher Details</title>
	</head>
	<body>
		<h5>${requestScope.message}</h5>
		<h5>Teacher's details : ${sessionScopes.user_details}</h5>
		<h5>
			<a href='<spring:url value="user/logout"/>'>Log Me Out</a>
		</h5>
	</body>
</html>