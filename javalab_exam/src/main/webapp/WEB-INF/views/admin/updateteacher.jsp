<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Update Teacher Details</title>
	</head>
	<body>
		<form:form method="post" modelAttribute="teacher">
		<table style="background-color: cyan; margin: auto;" border="1">
			<tr>
				<td>Update Name</td>
				<td><form:input path="name" /></td>
			</tr>
			<tr>
				<td>Update Password</td>
				<td><form:input type="password" path="password"/></td>
			</tr>

			<tr>
				<td><input type="submit" value="Update" /></td>
			</tr>
		</table>
	</form:form>
	</body>
</html>