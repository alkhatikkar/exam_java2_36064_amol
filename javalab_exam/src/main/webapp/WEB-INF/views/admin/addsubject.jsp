<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Add Subject</title>
	</head>
	<body>
		<form:form method="post" modelAttribute="subject">
		<table style="background-color: cyan; margin: auto;" border="1">
			<tr>
				<td>Enter Subject Name</td>
				<td><form:input type="text" path="name" /></td>
			</tr>
			<tr>
				<td>Enter Duration</td>
				<td><form:input type="number" path="duration" /></td>
			</tr>
			<tr>
				<td>Enter Course</td>
				<td><form:input path="course" /></td>
			</tr>
			<tr>
				<td>Enter Teacher Id</td>
				<td><form:input type="number" path="teacher" /></td>
			</tr>

			<tr>
				<td><input type="submit" value="Add Subject" /></td>
			</tr>
		</table>
	</form:form>
	</body>
</html>