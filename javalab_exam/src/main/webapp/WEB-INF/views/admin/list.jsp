<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5>${requestScope.message}</h5>
	<h5>Admin's details : ${sessionScope.user_details}</h5>

	<table style="background-color: cyan; margin: auto;" border="1">
		<caption>Teacher List</caption>
		<c:forEach var="t" items="${requestScope.teacher_list}">
			<tr>
				<td>${t.name}</td>
				<td>${t.email}</td>
				<td>${t.dob}</td>
				<td><a
					href="<spring:url value='/admin/update?tid=${t.id}'/>">Update</a></td>
				<td><a
					href="<spring:url value='/admin/delete?tid=${t.id}'/>">Delete</a></td>

				<td></td>
			</tr>
		</c:forEach>
	</table>
	<h5>
		<a href="<spring:url value='/admin/addsubject'/>">Add Subject</a>
	</h5>
	<h5>
		<a href="<spring:url value='/user/logout'/>">Log Me Out</a>
	</h5>
</body>
</html>