<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Index Page</title>
	</head>
	<body>
		<h1>Welcome To Sunbeam</h1>
		<h2><a href='<spring:url value="user/login"/>'>Login</a> here</h2>
	</body>
</html>