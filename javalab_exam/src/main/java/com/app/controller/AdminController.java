package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Subject;
import com.app.pojos.Teacher;
import com.app.service.ISubjectService;
import com.app.service.ITeacherService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private ITeacherService teacherService;
	
	@Autowired
	private ISubjectService subjectService;

	public AdminController() {
		
	}
	
	@GetMapping("/list")
	public String showTeacherList(Model map) {
		map.addAttribute("teacher_list", teacherService.getAllTeachers());
		return "/admin/list";
	}
	
	@GetMapping("/delete")
	public String removeVendorDetails(@RequestParam int tid,RedirectAttributes flashMap)
	{
		flashMap.addFlashAttribute("message", teacherService.deleteTeacherDetails(tid));
		return "redirect:/admin/list";
	}
	
	@GetMapping("/update")
	public String showTeacherFormPage(@RequestParam int tid,Teacher t) {
		t = teacherService.getTeacherById(tid);
		return "/admin/updateteacher";
	}
	
	@PostMapping("/update")
	public String processTeacherForm(Teacher t,RedirectAttributes flashMap)
	{
		flashMap.addFlashAttribute("message",teacherService.updateTeacherDetails(t));
		return "redirect:/admin/list";
	}
	
	@GetMapping("/addsubject")
	public String showRegForm(Subject s)
	{
		return "/admin/addsubject";
	}
	
	
	@PostMapping("/addsubject")
	public String processRegForm(Subject s,RedirectAttributes flashMap)
	{
		flashMap.addFlashAttribute("message",subjectService.addSubject(s));
		return "redirect:/admin/list";
	}
}
