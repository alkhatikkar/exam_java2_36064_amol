package com.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexPageController {
	
	public IndexPageController() {
		
	}
	
	@GetMapping("/")
	public String showIndexPage() {
		return "/index";
	}
}
