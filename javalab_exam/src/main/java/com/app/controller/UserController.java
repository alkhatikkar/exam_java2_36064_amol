package com.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.pojos.Role;
import com.app.pojos.Teacher;
import com.app.service.ITeacherService;

@Controller
@RequestMapping("/user")
public class UserController {

	@Autowired
	private ITeacherService teacherService;
	
	public UserController() {
		
	}
	
	@GetMapping("/login")
	public String showLoginPage() {
		return "/user/login";
	}
	
	@PostMapping("/login")
	public String processLoginForm(@RequestParam String email, @RequestParam String password, Model map,
			HttpSession session, RedirectAttributes flashMap) {
		System.out.println("in process login form " + email + " " + password);
		try {
			Teacher validatedUser = teacherService.authenticateUser(email, password);
			session.setAttribute("user_details", validatedUser);
			flashMap.addFlashAttribute("message", "Login Successful : " + validatedUser.getRole());
			if (validatedUser.getRole().equals(Role.ADMIN))
				return "redirect:/admin/list";
			return "redirect:/teacher/details";
		} catch (RuntimeException e) {
			System.out.println("err in process login form " + e);
			map.addAttribute("message", "Invalid Login , Pls retry....");
			return "/user/login";
		}
	}
	
	@GetMapping("/logout")
	public String userLogout(HttpSession hs, Model modelAttrMap, HttpServletResponse response,
			HttpServletRequest request) {
		modelAttrMap.addAttribute("details", hs.getAttribute("user_details"));
		hs.invalidate();
		response.setHeader("refresh", "5;url=" + request.getContextPath());

		return "/user/logout";
	}
}
