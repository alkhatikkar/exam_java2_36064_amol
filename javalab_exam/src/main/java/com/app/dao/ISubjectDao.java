package com.app.dao;

import com.app.pojos.Subject;

public interface ISubjectDao {
	String addSubject(Subject subject);
}
