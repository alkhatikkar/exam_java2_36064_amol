package com.app.dao;

import java.util.List;

import com.app.pojos.Teacher;

public interface ITeacherDao {
	Teacher authenticateUser(String email,String password);
	List<Teacher> fetchAllTeachers();
	String deleteTeacherDetails(int teacherId);
	String updateTeacherDetails(Teacher t);
	Teacher getTeacherById(int teacherId);
}
