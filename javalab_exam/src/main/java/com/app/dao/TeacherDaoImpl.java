package com.app.dao;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Role;
import com.app.pojos.Teacher;

@Repository
public class TeacherDaoImpl implements ITeacherDao {

	@Autowired
	private EntityManager manager;
	
	@Override
	public Teacher authenticateUser(String email, String password) {
		Teacher teacher = null;
		String jpql = "select t from Teacher t where t.email=:em and t.password=:pass";

		teacher = manager.createQuery(jpql, Teacher.class).setParameter("em", email).setParameter("pass", password)
				.getSingleResult();

		return teacher;
	}
	
	@Override
	public List<Teacher> fetchAllTeachers() {
		String jpql = "select t from Teacher t where t.role=:role";
		return manager.createQuery(jpql, Teacher.class).setParameter("role", Role.TEACHER).getResultList();
	}

	@Override
	public String deleteTeacherDetails(int teacherId) {
		String mesg="Teacher deletion failed";
		Teacher t = manager.find(Teacher.class, teacherId);

		if (t != null) {
			manager.remove(t);
			mesg="Teacher deleted successfully....";
		}
		return mesg;
	}

	@Override
	public String updateTeacherDetails(Teacher t) {
		String mesg="Teacher updation failed";
		manager.merge(t);
		mesg="Teacher details updated successfully....";
		return mesg;
	}

	@Override
	public Teacher getTeacherById(int teacherId) {
		Teacher t = manager.find(Teacher.class, teacherId);
		return t;
	}
}
