package com.app.dao;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Subject;

@Repository
public class SubjectDaoImpl implements ISubjectDao {

	@Autowired
	private EntityManager manager;
	
	@Override
	public String addSubject(Subject subject) {
		manager.persist(subject);
		return "Subject Added successfully";
	}
}
