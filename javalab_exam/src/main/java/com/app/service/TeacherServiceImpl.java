package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.ITeacherDao;
import com.app.pojos.Teacher;

@Service
@Transactional
public class TeacherServiceImpl implements ITeacherService {

	@Autowired
	private ITeacherDao teacherDao;

	@Override
	public Teacher authenticateUser(String email, String password) {
		return teacherDao.authenticateUser(email, password);
	}

	@Override
	public List<Teacher> getAllTeachers() {
		return teacherDao.fetchAllTeachers();
	}

	@Override
	public String deleteTeacherDetails(int teacherId) {
		return teacherDao.deleteTeacherDetails(teacherId);
	}

	@Override
	public String updateTeacherDetails(Teacher t) {
		return teacherDao.updateTeacherDetails(t);
	}

	@Override
	public Teacher getTeacherById(int teacherId) {
		return teacherDao.getTeacherById(teacherId);
	}
}
