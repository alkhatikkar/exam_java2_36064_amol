package com.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.app.pojos.Teacher;

@Service
public interface ITeacherService {
	Teacher authenticateUser(String email, String password);
	List<Teacher> getAllTeachers();
	String deleteTeacherDetails(int teacherId);
	String updateTeacherDetails(Teacher t);
	Teacher getTeacherById(int teacherId);
}
