package com.app.service;

import com.app.pojos.Subject;

public interface ISubjectService {
	String addSubject(Subject subject);
}
