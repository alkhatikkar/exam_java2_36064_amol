package com.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.dao.ISubjectDao;
import com.app.pojos.Subject;

@Service
public class SubjectServiceImpl implements ISubjectService {

	@Autowired
	private ISubjectDao subjectDao;
	
	@Override
	public String addSubject(Subject subject) {
		subjectDao.addSubject(subject);
		return null;
	}

}
